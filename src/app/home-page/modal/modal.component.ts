import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';

import {EventsService} from '../../shared/services/events.service';
import {Event} from '../../shared/models/event.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  @ViewChild('modal') public modal: ModalDirective;

  sub1: Subscription;
  form: FormGroup;
  today = new Date();

  constructor(private eventsService: EventsService) { }

  ngOnInit() {

    this.form = new FormGroup({
      'create_date': new FormControl({value: this.today, disabled: true}, [
        Validators.required
      ]),
      'state': new FormControl({value: 0, disabled: true}, [
        Validators.required
      ]),
      'estimate': new FormControl(null, [
        Validators.required
      ]),
      'description': new FormControl(null, [
        Validators.required
      ]),
      'title': new FormControl(null, [
        Validators.required
      ]),
      'priority': new FormControl(null, [
        Validators.required
      ]),
    });

  }

  showModal() {
    this.modal.show();
  }

  onSubmit() {

    let {create_date, state, estimate, description, title, priority } = this.form.getRawValue();

    const event = new Event(create_date, state, estimate, description, title, priority );

    console.log(event)

    this.sub1 = this.eventsService.addEvents(event)
      .subscribe((event: Event[]) => {
        this.form.reset();
      });

    this.modal.hide();

  }

  ngOnDestroy() {
    if (this.sub1) { this.sub1.unsubscribe(); }
  }



}
