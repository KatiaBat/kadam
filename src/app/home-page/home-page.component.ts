import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {EventsService} from '../shared/services/events.service';
import {Subscription} from 'rxjs/Subscription';
import { Event } from '../shared/models/event.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {

  events;
  isLoaded = false;
  sbscr: Subscription;
  modalRef: BsModalRef;
  create_date;

  constructor(private eventsService: EventsService, private modalService: BsModalService) { }

  ngOnInit() {

    this.sbscr = this.eventsService.getEvents().subscribe((data: Event[]) => {

      this.events = data;
      this.isLoaded = true;
    });

  }

  delete (template: TemplateRef<any>, create_date) {
    this.modalRef = this.modalService.show(template);

    this.create_date = create_date;
  }

  onDelete(create_date) {

    console.log(this.create_date);
    this.sbscr = this.eventsService.deleteEvents(this.create_date)
      .subscribe((event: Event[]) => {

      });

    this.modalRef.hide();
  }

  ngOnDestroy() {
    if (this.sbscr) { this.sbscr.unsubscribe(); }
  }

}
