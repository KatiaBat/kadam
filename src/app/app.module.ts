import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { StatisticPageComponent } from './statistic-page/statistic-page.component';
import { UsersPageComponent } from './users-page/users-page.component';
import { AppsPageComponent } from './apps-page/apps-page.component';
import { MeetingsPageComponent } from './meetings-page/meetings-page.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {AppRoutingModule} from './app-routing.module';
import {BsDropdownModule, ModalModule} from 'ngx-bootstrap';
import {EventsService} from './shared/services/events.service';
import {HttpModule} from '@angular/http';
import { ModalComponent } from './home-page/modal/modal.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    StatisticPageComponent,
    UsersPageComponent,
    AppsPageComponent,
    MeetingsPageComponent,
    HeaderComponent,
    ModalComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [EventsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
