import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {StatisticPageComponent} from './statistic-page/statistic-page.component';
import {UsersPageComponent} from './users-page/users-page.component';
import {AppsPageComponent} from './apps-page/apps-page.component';
import {MeetingsPageComponent} from './meetings-page/meetings-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent, pathMatch: 'full'},
  {path: 'statistic', component: StatisticPageComponent},
  {path: 'users', component: UsersPageComponent},
  {path: 'apps', component: AppsPageComponent},
  {path: 'meetings', component: MeetingsPageComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
