export class Event {
  constructor(
    public create_date: string,
    public state: number,
    public estimate: number,
    public title: string,
    public description: string,
    public priority: number
  ) {}
}
