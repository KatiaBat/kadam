import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { BaseApi } from '../../shared/core/base-api';
import { Event } from '../models/event.model';

@Injectable()
export class EventsService extends BaseApi {
  constructor(public http: Http) {
    super(http);
  }

  getEvents(): Observable<Event[]> {
    return this.get('events');
  }

  addEvents(event): Observable<Event[]> {
    return this.post('events', event);
  }

  deleteEvents(event): Observable<Event[]> {
    return this.delete('events');
  }

}
